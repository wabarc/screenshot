module github.com/wabarc/screenshot

go 1.16

require (
	github.com/chromedp/cdproto v0.0.0-20210323015217-0942afbea50e
	github.com/chromedp/chromedp v0.6.10
	github.com/wabarc/helper v0.0.0-20210127120855-10af37cc2616
	golang.org/x/sys v0.0.0-20210415045647-66c3f260301c // indirect
)
