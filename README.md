# screenshot

Screenshot is a project that capture and save webpage as image using [chromedp](https://github.com/chromedp/chromedp).

This repository is a *work in progress*.

## Prerequisite

- Chrome/Chromium

## Installation

From source:

```sh
$ go get github.com/wabarc/screenshot
```

From [gobinaries.com](https://gobinaries.com):

```sh
$ curl -sf https://gobinaries.com/wabarc/screenshot | sh
```

From [releases](https://github.com/wabarc/screenshot/releases)

## Credits

- [chromedp](https://github.com/chromedp)

## License

This software is released under the terms of the GNU General Public License v3.0. See the [LICENSE](https://github.com/wabarc/screenshot/blob/main/LICENSE) file for details.
